<?php

class testA {
    var $test1 = "b";
    
    // tr1で変更
    public function bbb() {
        return "bbb";
    }

    // tr2で変更
    public function ccc() {
        return "ccc";
    }

    // tr3で追加
    public function tr3() {
        echo "tr3";
        return "ccc";
    }

    // tr4で追加
    public function tr4() {
        return "ccc";
    }

    // tr4aで追加
    public function tr4a() {
        return "ccc";
    }
}